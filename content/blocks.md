+++
date = "2018-04-18T21:41:12Z"
test-list = ["baz", "bar", "foo"]
title = "Blocks "
type = "page"
[menu.main]
weight = 5
[[blocks]]
background_image = "/uploads/2018/04/18/sawmill-splash.png"
background_style = "Brand Color"
heading = "Blocks"
template = "hero-section"
[[blocks]]
template = "body-copy"
[[blocks]]
background_style = "Dark"
heading = "Media Feature"
image_position = "Right"
image_shadow = true
template = "media-feature"
[[blocks]]
background_style = "Light"
heading = "Choose Your Style"
image_position = "Left"
image_shadow = true
subheading = "Media Feature blocks can have light, dark, or brand-color backgrounds."
template = "media-feature"
[[blocks]]
background_style = "Light"
heading = "Multiple Image Alignment Options"
image_position = "Right"
image_shadow = true
subheading = "Left, Right, Top, or Bottom"
template = "media-feature"
[[blocks]]
background_style = "Light"
content = "Hero and Media Feature blocks can be optionally punctuated with a call to action button."
heading = "Call to Action Buttons"
image_position = "Right"
image_shadow = true
template = "media-feature"
[[blocks]]
content = "## Pure Markdown Sections\n\nInsert markdown sections in between other block types for maximum flexibility."
template = "body-copy"
[[blocks]]
background_style = "Brand Color"
content = "A dedicated call to action section to grab attention."
heading = "Call To Action Block"
image_position = "Left"
image_shadow = true
template = "media-feature"
[[blocks]]
button_text = "View on Github"
button_url = "/"
heading = "More to Come"
subheading = "Suggest more blocks or send us a PR"
template = "call-to-action"

+++
