+++
title = "Sawmill: A Modular Layout Builder for Hugo and Forestry.io"
type = "page"
[[blocks]]
background_image = "/uploads/2018/04/18/sawmill-splash.png"
background_style = "Dark"
button_text = "test"
button_url = ""
heading = "Sawmill"
size = "Big"
subheading = "A Modular Layout Builder for Hugo and [Forestry.io](https://forestryio)"
template = "hero-section"
[[blocks]]
content = "**Sawmill** is a simplistic Hugo theme built to take advantage of \\[Forestry CMS\\](https://forestry.io)'s Blocks feature.\n\nSawmill provides some starter block templates..."
template = "body-copy"
[[blocks]]
background_style = "Brand Color"
heading = "Features"
size = "Small"
template = "hero-section"
[[blocks]]
background_style = "Light"
button_text = ""
button_url = ""
content = "[Bulma](https://bulma.io/) is a modern, responsive CSS framework with a flexbox-based grid system. "
heading = "Built With Bulma"
image = "/uploads/2018/04/18/bulma-logo.png"
image_position = "Left"
template = "media-feature"
[[blocks]]
background_style = "Light"
content = "The theme's primary color and logo are customizable from your `config.toml` file."
heading = "Customizable Branding"
image_position = "Right"
template = "media-feature"
[[blocks]]
background_style = "Dark"
content = "This theme is designed to work out-of-the-box with [forestry.io's](https://forestry.io) **Blocks** feature. Simply copy the the theme's `.forestry/` directory into the top level of your project, or get started even faster with the [starter project](/)"
heading = "Made for Forestry"
image = "/uploads/2018/04/18/blocks_ss.png"
image_position = "Left"
image_shadow = true
template = "media-feature"
[[blocks]]
button_text = "Fork on Github"
button_url = "/"
heading = "Let's Get Started"
subheading = ""
template = "call-to-action"

+++
# Home sweet home